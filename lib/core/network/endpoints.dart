class Endpoints {
  Endpoints._();
  // base url
  static const String baseUrl = "http://numbersapi.com/";

  // receiveTimeout
  static const int receiveTimeout = 5000;

  // connectTimeout
  static const int connectionTimeout = 3000;

  static const String getTriviaRandom = baseUrl + "random/year?json";
}