import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:flutter_clean_arch/core/network/network_info.dart';
import 'package:inject/inject.dart';
import 'package:shared_preferences/shared_preferences.dart';

@module
class AppModule {

  @provide
  @singleton
  @asynchronous
  Future<SharedPreferences> provideSharedPreferences() {
    return SharedPreferences.getInstance();
  }

  @provide
  @singleton
  DataConnectionChecker provideDataConnectionChecker() {
    return DataConnectionChecker();
  }

  @provide
  @singleton
  NetworkInfo provideNetworkInfo(DataConnectionChecker connectionChecker) {
    return NetworkInfoImpl(connectionChecker);
  }
}