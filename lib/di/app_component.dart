import 'package:flutter_clean_arch/data/di/network_module.dart';
import 'package:flutter_clean_arch/data/di/repository_module.dart';
import 'package:flutter_clean_arch/di/app_module.dart';
import 'package:flutter_clean_arch/domain/reposioties/number_trivia_repository.dart';
import 'package:flutter_clean_arch/features/di/bloc_module.dart';
import 'package:flutter_clean_arch/features/pages/number_trivia_page.dart';
import 'package:flutter_clean_arch/main.dart';
import 'package:inject/inject.dart';

import 'app_component.inject.dart' as g;

@Injector(const [AppModule, NetworkModule, RepositoryModule, BlocModule])
abstract class AppComponent {
  @provide
  MyApp get app;

  @provide
  NumberTriviaPage get page;

  static Future<AppComponent> create(
      AppModule appModule,
      NetworkModule networkModule,
      RepositoryModule repositoryModule,
      BlocModule blocModule
      ) async {
    return await g.AppComponent$Injector.create(appModule, networkModule, repositoryModule, blocModule);
  }

  @provide
  NumberTriviaRepository get repository;
}
