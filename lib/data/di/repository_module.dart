import 'package:flutter_clean_arch/core/network/network_info.dart';
import 'package:flutter_clean_arch/core/network/restclient.dart';
import 'package:flutter_clean_arch/data/repositories/number_trivia_repository_impl.dart';
import 'package:flutter_clean_arch/domain/reposioties/number_trivia_repository.dart';
import 'package:inject/inject.dart';

@module
class RepositoryModule {

  @provide
  @singleton
  NumberTriviaRepository providerNumberTriviaRepository(NetworkInfo networkInfo, RestClient client) {
    return NumberTriviaRepositoryImpl(networkInfo, client);
  }
}