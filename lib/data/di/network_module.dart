import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_clean_arch/core/network/endpoints.dart';
import 'package:flutter_clean_arch/core/network/restclient.dart';
import 'package:inject/inject.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

@module
class NetworkModule {
  // ignore: non_constant_identifier_names
  final String TAG = "NetworkModule";

  // DI Providers:--------------------------------------------------------------
  /// A singleton dio provider.
  ///
  /// Calling it multiple times will return the same instance.
  @provide
  @singleton
  Dio provideDio() {
    final dio = Dio();

    dio
      ..options.baseUrl = Endpoints.baseUrl
      ..options.connectTimeout = Endpoints.connectionTimeout
      ..options.receiveTimeout = Endpoints.receiveTimeout
      ..options.headers = {'Content-Type': 'application/json; charset=utf-8'};
      // ..interceptors.add(PrettyDioLogger(
      //   requestHeader: true,
      //   requestBody: true,
      //   responseBody: true,
      //   responseHeader: true,
      //   compact: false,
      // ));

    // if (kDebugMode) {
    //   dio.interceptors.add(PrettyDioLogger(
    //     requestHeader: true,
    //     requestBody: true,
    //     responseBody: true,
    //     responseHeader: true,
    //     compact: false,
    //   ));
    // }
    // ..interceptors.add(
      //   InterceptorsWrapper(
      //     onRequest: (Options options) async {
      //       // getting shared pref instance
      //       var prefs = await SharedPreferences.getInstance();
      //     },
      //   ),
      // );

    return dio;
  }

  /// A singleton dio_client provider.
  ///
  /// Calling it multiple times will return the same instance.
  @provide
  @singleton
  RestClient provideRestClient() => RestClient();

  // Api Providers:-------------------------------------------------------------
  // Define all your api providers here
  /// A singleton post_api provider.
  ///
  /// Calling it multiple times will return the same instance.
  // @provide
  // @singleton
  // PostApi providePostApi(DioClient dioClient, RestClient restClient) =>
  //     PostApi(dioClient, restClient);
// Api Providers End:---------------------------------------------------------

}