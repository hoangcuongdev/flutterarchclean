import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:flutter_clean_arch/core/error/failures.dart';
import 'package:flutter_clean_arch/core/network/endpoints.dart';
import 'package:flutter_clean_arch/core/network/network_info.dart';
import 'package:flutter_clean_arch/core/network/restclient.dart';
import 'package:flutter_clean_arch/data/models/number_trivia_response.dart';
import 'package:flutter_clean_arch/domain/entities/number_trivia.dart';
import 'package:flutter_clean_arch/domain/reposioties/number_trivia_repository.dart';

class NumberTriviaRepositoryImpl extends NumberTriviaRepository {
  final NetworkInfo networkInfo;
  final RestClient _client;

  NumberTriviaRepositoryImpl(this.networkInfo, this._client);

  @override
  Future<Either<Failure, NumberTrivia>> getConcreteNumberTrivia(int number) {
    // TODO: implement getConcreteNumberTrivia
    throw UnimplementedError();
  }

  @override
  Future<Either<Failure, NumberTrivia>> getRandomNumberTrivia() async {
    if (await networkInfo.isConnected) {
      try {
        final response = await _client.get(Endpoints.getTriviaRandom);
        return Right(NumberTriviaResponse.fromJson(response));
      } catch (e) {
        return Left(CacheFailure());
      }
    } else {
      return Left(ServerFailure());
    }
  }
}