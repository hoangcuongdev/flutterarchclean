import 'package:flutter/cupertino.dart';
import 'package:flutter_clean_arch/domain/entities/number_trivia.dart';
import 'package:json_annotation/json_annotation.dart';

part 'number_trivia_response.g.dart';

@JsonSerializable(nullable: true)
class NumberTriviaResponse extends NumberTrivia {
  NumberTriviaResponse({
    @required String text,
    @required int number
  }) : super(text: text, number: number);

  factory NumberTriviaResponse.fromJson(Map<String, dynamic> json) => _$NumberTriviaResponseFromJson(json);
  Map<String, dynamic> toJson() => _$NumberTriviaResponseToJson(this);
}
