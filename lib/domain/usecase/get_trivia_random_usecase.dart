import 'package:dartz/dartz.dart';
import 'package:flutter_clean_arch/core/error/failures.dart';
import 'package:flutter_clean_arch/domain/entities/number_trivia.dart';
import 'package:flutter_clean_arch/domain/reposioties/number_trivia_repository.dart';
import 'package:flutter_clean_arch/domain/usecase/usecase.dart';
import 'package:inject/inject.dart';

class GetRandomNumberTriviaUseCase implements UseCase<NumberTrivia, NoParams> {
  final NumberTriviaRepository numberTriviaRepository;

  @provide
  GetRandomNumberTriviaUseCase(this.numberTriviaRepository);

  @override
  Future<Either<Failure, NumberTrivia>> call(NoParams params) async {
    return await numberTriviaRepository.getRandomNumberTrivia();
  }
}
