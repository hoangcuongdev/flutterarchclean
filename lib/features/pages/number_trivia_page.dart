import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_clean_arch/features/bloc/bloc.dart';
import 'package:flutter_clean_arch/features/widgets/loading_widget.dart';
import 'package:flutter_clean_arch/features/widgets/message_display_widget.dart';
import 'package:flutter_clean_arch/features/widgets/trivia_controls_widget.dart';
import 'package:flutter_clean_arch/features/widgets/trivia_display_widget.dart';
import 'package:inject/inject.dart';

@provide
class NumberTriviaPage extends StatelessWidget {
  final NumberTriviaBLoc bLoc;

  NumberTriviaPage(this.bLoc);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Clean Arch"),
      ),
      body: SingleChildScrollView(
        child: buildBody(context, bLoc),
      ),
    );
  }
}

BlocProvider<NumberTriviaBLoc> buildBody(
    BuildContext context, NumberTriviaBLoc bLoc) {
  return BlocProvider(
    create: (_) => bLoc,
    child: Center(
      child: Padding(
        padding: const EdgeInsets.all(10),
        child: Column(
          children: <Widget>[
            SizedBox(height: 10),
            BlocBuilder<NumberTriviaBLoc, NumberTriviaState>(
                builder: (context, state) {
              if (state is EmptyState) {
                return MessageDisplayWidget(message: "Start searching");
              } else if (state is LoadingState) {
                return LoadingWidget();
              } else if (state is LoadedState) {
                return TriviaDisplayWidget(numberTrivia: state.trivia);
              } else {
                return MessageDisplayWidget(message: "Nooo");
              }
            }),
            SizedBox(
              height: 20,
            ),
            TriviaControlsWidget()
          ],
        ),
      ),
    ),
  );
}
