
import 'package:dartz/dartz.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_clean_arch/core/error/failures.dart';
import 'package:flutter_clean_arch/domain/entities/number_trivia.dart';
import 'package:flutter_clean_arch/domain/usecase/get_trivia_random_usecase.dart';
import 'package:flutter_clean_arch/domain/usecase/usecase.dart';
import 'package:flutter_clean_arch/features/bloc/bloc.dart';

const String _SERVER_FAILURE_MESSAGE = 'Server Failure';
const String _CACHE_FAILURE_MESSAGE = 'Cache Failure';

class NumberTriviaBLoc extends Bloc<NumberTriviaEvent, NumberTriviaState> {
  final GetRandomNumberTriviaUseCase getRandomNumberTrivia;

  NumberTriviaBLoc({@required GetRandomNumberTriviaUseCase random}) : assert(random != null),
        getRandomNumberTrivia = random,
        super(EmptyState());


  Stream<NumberTriviaState> _eitherLoadedOrErrorState(
      Either<Failure, NumberTrivia> failureOrTrivia,
      ) async* {
    yield failureOrTrivia.fold(
          (failure) => ErrorState(message: _mapFailureToMessage(failure)),
          (trivia) => LoadedState(trivia: trivia),
    );
  }

  String _mapFailureToMessage(Failure failure) {
    switch (failure.runtimeType) {
      case ServerFailure:
        return _SERVER_FAILURE_MESSAGE;
      case CacheFailure:
        return _CACHE_FAILURE_MESSAGE;
      default:
        return 'Unexpected error';
    }
  }

  @override
  Stream<NumberTriviaState> mapEventToState(NumberTriviaEvent event) async* {
      if(event is GetTriviaForRandomNumberEvent) {
        yield LoadingState();
        final failureOrTrivia = await getRandomNumberTrivia(NoParams());
        yield* _eitherLoadedOrErrorState(failureOrTrivia);
      }
  }
}