import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

@immutable
abstract class NumberTriviaEvent extends Equatable {

  @override
  List<Object> get props  => [];
}

class GetTriviaForRandomNumberEvent extends NumberTriviaEvent {}