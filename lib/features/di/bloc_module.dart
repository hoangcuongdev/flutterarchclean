import 'package:flutter/material.dart';
import 'package:flutter_clean_arch/domain/reposioties/number_trivia_repository.dart';
import 'package:flutter_clean_arch/domain/usecase/get_trivia_random_usecase.dart';
import 'package:flutter_clean_arch/features/bloc/bloc.dart';
import 'package:flutter_clean_arch/features/pages/number_trivia_page.dart';
import 'package:inject/inject.dart';

@module
class BlocModule {
  @provide
  GetRandomNumberTriviaUseCase provideGetRandomNumberTriviaUseCase(NumberTriviaRepository repository) {
    return GetRandomNumberTriviaUseCase(repository);
  }
  @provide
  NumberTriviaBLoc provideNumberTriviaBLoc(GetRandomNumberTriviaUseCase useCase) {
      return NumberTriviaBLoc(random: useCase);
  }

  @provide
  NumberTriviaPage provideNumberTriviaPage(NumberTriviaBLoc bLoc) {
    return NumberTriviaPage(bLoc);
  }
}